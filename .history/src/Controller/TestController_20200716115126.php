<?php

namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller{

    /**
     * @Route("/", name="index")
     */
    public function index(){
            return $this->render('dashboard/dashboard.html.twig');
    }

    /**
     * @Route("/produits", name="produits")
     */
    public function produits(){
        return $this->render('produits/produits.html.twig');
    }

    /**
     * @Route("/type", name="produits_types")
     */
    public function Typeproduits(){
        return $this->render('typeproduit/typeProduit.html.twig');
    }

    /**
     * @Route("/marque", name="produit_marque")
     */
    public function marque(){
        return $this->render('marque/marque.html.twig');
    }

    /**
     * @Route("/gamme", name="produits_gamme")
     */
    public function gamme(){
        return $this->render('gamme/gamme.html.twig');
    }

    /**
     * @Route("/unite", name="produits_unite")
     */
    public function uniteMesure(){
        return $this->render('uniteMesure/uniteMesure.html.twig');
    }
    
}