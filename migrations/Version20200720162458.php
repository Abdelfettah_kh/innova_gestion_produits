<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200720162458 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE produits CHANGE etp_id etp_id INT DEFAULT NULL, CHANGE gamme_id gamme_id INT DEFAULT NULL, CHANGE marque_id marque_id INT DEFAULT NULL, CHANGE tpd_id tpd_id INT DEFAULT NULL, CHANGE pdt_description pdt_description VARCHAR(255) DEFAULT NULL, CHANGE pdt_image pdt_image VARCHAR(50) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE produits CHANGE etp_id etp_id INT DEFAULT NULL, CHANGE gamme_id gamme_id INT DEFAULT NULL, CHANGE marque_id marque_id INT DEFAULT NULL, CHANGE tpd_id tpd_id INT DEFAULT NULL, CHANGE pdt_description pdt_description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE pdt_image pdt_image VARCHAR(50) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
