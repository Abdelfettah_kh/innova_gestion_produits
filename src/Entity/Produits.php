<?php

namespace App\Entity;

use App\Repository\ProduitsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProduitsRepository::class)
 */
class Produits
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pdt_libelle;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_At;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Updated_At;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $pdt_reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pdt_description;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $pdt_image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pdt_descint;

    /**
     * @ORM\ManyToMany(targetEntity=UniteMesure::class, inversedBy="pdt_id")
     */
    private $um;

    /**
     * @ORM\ManyToOne(targetEntity=EtatProduit::class, inversedBy="pdt_id")
     */
    private $etp;

    /**
     * @ORM\ManyToOne(targetEntity=GammeProduit::class, inversedBy="pdt_id")
     */
    private $gamme;

    /**
     * @ORM\ManyToOne(targetEntity=MarqueProduit::class, inversedBy="pdt_id")
     */
    private $marque;

    /**
     * @ORM\ManyToOne(targetEntity=TypeProduit::class, inversedBy="produit")
     */
    private $tpd;

    /**
     * @ORM\OneToMany(targetEntity=ProduitExtension::class, mappedBy="produit")
     */
    private $produit;



    public function __construct()
    {
        $this->um_id = new ArrayCollection();
        $this->produit = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPdtLibelle()
    {
        return $this->pdt_libelle;
    }

    /**
     * @param mixed $pdt_libelle
     */
    public function setPdtLibelle($pdt_libelle): void
    {
        $this->pdt_libelle = $pdt_libelle;
    }

    /**
     * @return mixed
     */
    public function getPdtDescint()
    {
        return $this->pdt_descint;
    }

    /**
     * @param mixed $pdt_descint
     */
    public function setPdtDescint($pdt_descint): void
    {
        $this->pdt_descint = $pdt_descint;
    }

    /**
     * @return mixed
     */
    public function getUm()
    {
        return $this->um;
    }

    /**
     * @param mixed $um
     */
    public function setUm($um): void
    {
        $this->um = $um;
    }

    /**
     * @return mixed
     */
    public function getEtp()
    {
        return $this->etp;
    }

    /**
     * @param mixed $etp
     */
    public function setEtp($etp): void
    {
        $this->etp = $etp;
    }

    /**
     * @return mixed
     */
    public function getGamme()
    {
        return $this->gamme;
    }

    /**
     * @param mixed $gamme
     */
    public function setGamme($gamme): void
    {
        $this->gamme = $gamme;
    }

    /**
     * @return mixed
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * @param mixed $marque
     */
    public function setMarque($marque): void
    {
        $this->marque = $marque;
    }

    /**
     * @return mixed
     */
    public function getTpd()
    {
        return $this->tpd;
    }

    /**
     * @param mixed $tpd
     */
    public function setTpd($tpd): void
    {
        $this->tpd = $tpd;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_At;
    }

    public function setCreatedAt(\DateTimeInterface $created_At): self
    {
        $this->created_At = $created_At;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->Updated_At;
    }

    public function setUpdatedAt(\DateTimeInterface $Updated_At): self
    {
        $this->Updated_At = $Updated_At;

        return $this;
    }

    public function getPdtReference(): ?string
    {
        return $this->pdt_reference;
    }

    public function setPdtReference(string $pdt_reference): self
    {
        $this->pdt_reference = $pdt_reference;

        return $this;
    }

    public function getPdtDescription(): ?string
    {
        return $this->pdt_description;
    }

    public function setPdtDescription(?string $pdt_description): self
    {
        $this->pdt_description = $pdt_description;

        return $this;
    }

    public function getPdtImage(): ?string
    {
        return $this->pdt_image;
    }

    public function setPdtImage(?string $pdt_image): self
    {
        $this->pdt_image = $pdt_image;

        return $this;
    }



    /**
     * @return Collection|UniteMesure[]
     */
    public function getUmId(): Collection
    {
        return $this->um_id;
    }

    public function addUmId(UniteMesure $umId): self
    {
        if (!$this->um_id->contains($umId)) {
            $this->um_id[] = $umId;
        }

        return $this;
    }

    public function removeUmId(UniteMesure $umId): self
    {
        if ($this->um_id->contains($umId)) {
            $this->um_id->removeElement($umId);
        }

        return $this;
    }

    /**
     * @return Collection|ProduitExtension[]
     */
    public function getProduit(): Collection
    {
        return $this->produit;
    }

    public function addProduit(ProduitExtension $produit): self
    {
        if (!$this->produit->contains($produit)) {
            $this->produit[] = $produit;
            $produit->setProduit($this);
        }

        return $this;
    }

    public function removeProduit(ProduitExtension $produit): self
    {
        if ($this->produit->contains($produit)) {
            $this->produit->removeElement($produit);
            // set the owning side to null (unless already changed)
            if ($produit->getProduit() === $this) {
                $produit->setProduit(null);
            }
        }

        return $this;
    }


    public function __toString(){
        // to show the name of the Category in the select
        return $this->pdt_libelle;
        // to show the id of the Category in the select
        // return $this->id;
    }




}
