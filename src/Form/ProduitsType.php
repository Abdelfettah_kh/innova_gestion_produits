<?php

namespace App\Form;

use App\Entity\Produits;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pdt_libelle')
            ->add('pdt_reference')
            ->add('pdt_description')
            ->add('pdt_image')
            ->add('pdt_desc_int')
            ->add('um')
            ->add('etp')
            ->add('gamme')
            ->add('marque')
            ->add('tpd')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Produits::class,
        ]);
    }
}
