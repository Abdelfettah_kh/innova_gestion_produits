<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200715160150 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE produits_unite_mesure (produits_id INT NOT NULL, unite_mesure_id INT NOT NULL, INDEX IDX_75F8E506CD11A2CF (produits_id), INDEX IDX_75F8E506C75A06BF (unite_mesure_id), PRIMARY KEY(produits_id, unite_mesure_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE produits_unite_mesure ADD CONSTRAINT FK_75F8E506CD11A2CF FOREIGN KEY (produits_id) REFERENCES produits (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE produits_unite_mesure ADD CONSTRAINT FK_75F8E506C75A06BF FOREIGN KEY (unite_mesure_id) REFERENCES unite_mesure (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE produits ADD etp_id_id INT DEFAULT NULL, ADD gamme_id_id INT DEFAULT NULL, ADD marque_id_id INT DEFAULT NULL, ADD pdt_reference VARCHAR(50) NOT NULL, ADD pdt_description VARCHAR(255) DEFAULT NULL, ADD pdt_image VARCHAR(50) DEFAULT NULL, ADD pdt_desc_int VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8C70314C6C FOREIGN KEY (etp_id_id) REFERENCES etat_produit (id)');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8C57BF3EAA FOREIGN KEY (gamme_id_id) REFERENCES gamme_produit (id)');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8C30964F9C FOREIGN KEY (marque_id_id) REFERENCES marque_produit (id)');
        $this->addSql('CREATE INDEX IDX_BE2DDF8C70314C6C ON produits (etp_id_id)');
        $this->addSql('CREATE INDEX IDX_BE2DDF8C57BF3EAA ON produits (gamme_id_id)');
        $this->addSql('CREATE INDEX IDX_BE2DDF8C30964F9C ON produits (marque_id_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE produits_unite_mesure');
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8C70314C6C');
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8C57BF3EAA');
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8C30964F9C');
        $this->addSql('DROP INDEX IDX_BE2DDF8C70314C6C ON produits');
        $this->addSql('DROP INDEX IDX_BE2DDF8C57BF3EAA ON produits');
        $this->addSql('DROP INDEX IDX_BE2DDF8C30964F9C ON produits');
        $this->addSql('ALTER TABLE produits DROP etp_id_id, DROP gamme_id_id, DROP marque_id_id, DROP pdt_reference, DROP pdt_description, DROP pdt_image, DROP pdt_desc_int');
    }
}
