<?php

namespace App\Controller;

use Symfony\Component\Routing\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller{

    /**
     * @Route("/", name="index")
     */
    public function index(){
            return $this->render('blank.html.twig');
    }

    
}