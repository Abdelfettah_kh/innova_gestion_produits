<?php

namespace App\Entity;

use App\Repository\ProduitExtensionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProduitExtensionRepository::class)
 */
class ProduitExtension
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $par_valeur;

    /**
     * @ORM\ManyToOne(targetEntity=Produits::class, inversedBy="produit")
     * @ORM\JoinColumn(nullable=false)
     */
    private $produit;

    /**
     * @ORM\ManyToOne(targetEntity=Parametres::class, inversedBy="param")
     * @ORM\JoinColumn(nullable=false)
     */
    private $parametres;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParValeur(): ?string
    {
        return $this->par_valeur;
    }

    public function setParValeur(string $par_valeur): self
    {
        $this->par_valeur = $par_valeur;

        return $this;
    }

    public function getProduit(): ?Produits
    {
        return $this->produit;
    }

    public function setProduit(?Produits $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getParametres(): ?Parametres
    {
        return $this->parametres;
    }

    public function setParametres(?Parametres $parametres): self
    {
        $this->parametres = $parametres;

        return $this;
    }
}
