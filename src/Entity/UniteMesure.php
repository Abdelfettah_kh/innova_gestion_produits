<?php

namespace App\Entity;

use App\Repository\UniteMesureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UniteMesureRepository::class)
 */
class UniteMesure
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\ManyToMany(targetEntity=Produits::class, mappedBy="um")
     */
    private $pdt_id;

    public function __construct()
    {
        $this->pdt_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|Produits[]
     */
    public function getPdtId(): Collection
    {
        return $this->pdt_id;
    }

    public function addPdtId(Produits $pdtId): self
    {
        if (!$this->pdt_id->contains($pdtId)) {
            $this->pdt_id[] = $pdtId;
            $pdtId->addUmId($this);
        }

        return $this;
    }

    public function removePdtId(Produits $pdtId): self
    {
        if ($this->pdt_id->contains($pdtId)) {
            $this->pdt_id->removeElement($pdtId);
            $pdtId->removeUmId($this);
        }

        return $this;
    }


    public function __toString(){
        // to show the name of the Category in the select
        return $this->libelle;
        // to show the id of the Category in the select
        // return $this->id;
    }
}
