<?php

namespace App\Entity;

use App\Repository\GammeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GammeRepository::class)
 */
class GammeProduit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Libelle;

    /**
     * @ORM\OneToMany(targetEntity=Produits::class, mappedBy="gamme")
     */
    private $pdt_id;

    public function __construct()
    {
        $this->pdt_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->Libelle;
    }

    public function setLibelle(string $Libelle): self
    {
        $this->Libelle = $Libelle;

        return $this;
    }

    /**
     * @return Collection|Produits[]
     */
    public function getPdtId(): Collection
    {
        return $this->pdt_id;
    }

    public function addPdtId(Produits $pdtId): self
    {
        if (!$this->pdt_id->contains($pdtId)) {
            $this->pdt_id[] = $pdtId;
            $pdtId->setGamme($this);
        }

        return $this;
    }

    public function removePdtId(Produits $pdtId): self
    {
        if ($this->pdt_id->contains($pdtId)) {
            $this->pdt_id->removeElement($pdtId);
            // set the owning side to null (unless already changed)
            if ($pdtId->getGamme() === $this) {
                $pdtId->setGamme(null);
            }
        }

        return $this;
    }

    public function __toString(){
        // to show the name of the Category in the select
        return $this->Libelle;
        // to show the id of the Category in the select
        // return $this->id;
    }
}
