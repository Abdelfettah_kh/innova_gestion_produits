<?php

namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller{

    /**
     * @Route("/", name="index")
     */
    public function index(){
            return $this->render('dashboard.html.twig');
    }

    /**
     * @Route("/produits", name="produits")
     */
    public function index(){
        return $this->render('produits.html.twig');
}

    
}