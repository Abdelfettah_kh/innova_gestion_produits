<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200715161619 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE produits ADD type_id INT DEFAULT NULL, CHANGE etp_id_id etp_id_id INT DEFAULT NULL, CHANGE gamme_id_id gamme_id_id INT DEFAULT NULL, CHANGE marque_id_id marque_id_id INT DEFAULT NULL, CHANGE pdt_description pdt_description VARCHAR(255) DEFAULT NULL, CHANGE pdt_image pdt_image VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8CC54C8C93 FOREIGN KEY (type_id) REFERENCES type_produit (id)');
        $this->addSql('CREATE INDEX IDX_BE2DDF8CC54C8C93 ON produits (type_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8CC54C8C93');
        $this->addSql('DROP INDEX IDX_BE2DDF8CC54C8C93 ON produits');
        $this->addSql('ALTER TABLE produits DROP type_id, CHANGE etp_id_id etp_id_id INT DEFAULT NULL, CHANGE gamme_id_id gamme_id_id INT DEFAULT NULL, CHANGE marque_id_id marque_id_id INT DEFAULT NULL, CHANGE pdt_description pdt_description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE pdt_image pdt_image VARCHAR(50) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
