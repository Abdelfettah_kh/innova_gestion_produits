<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200715164221 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE produit_extension ADD produit_id INT NOT NULL, ADD parametres_id INT NOT NULL');
        $this->addSql('ALTER TABLE produit_extension ADD CONSTRAINT FK_1B20639BF347EFB FOREIGN KEY (produit_id) REFERENCES produits (id)');
        $this->addSql('ALTER TABLE produit_extension ADD CONSTRAINT FK_1B20639B44AEE5AE FOREIGN KEY (parametres_id) REFERENCES parametres (id)');
        $this->addSql('CREATE INDEX IDX_1B20639BF347EFB ON produit_extension (produit_id)');
        $this->addSql('CREATE INDEX IDX_1B20639B44AEE5AE ON produit_extension (parametres_id)');
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8C30964F9C');
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8C57BF3EAA');
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8C70314C6C');
        $this->addSql('DROP INDEX IDX_BE2DDF8C70314C6C ON produits');
        $this->addSql('DROP INDEX IDX_BE2DDF8C57BF3EAA ON produits');
        $this->addSql('DROP INDEX IDX_BE2DDF8C30964F9C ON produits');
        $this->addSql('ALTER TABLE produits ADD etp_id_id INT DEFAULT NULL, ADD gamme_id_id INT DEFAULT NULL, ADD marque_id_id INT DEFAULT NULL, DROP etp_id, DROP gamme_id, DROP marque_id, CHANGE type_id type_id INT DEFAULT NULL, CHANGE pdt_description pdt_description VARCHAR(255) DEFAULT NULL, CHANGE pdt_image pdt_image VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8C30964F9C FOREIGN KEY (marque_id_id) REFERENCES marque_produit (id)');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8C57BF3EAA FOREIGN KEY (gamme_id_id) REFERENCES gamme_produit (id)');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8C70314C6C FOREIGN KEY (etp_id_id) REFERENCES etat_produit (id)');
        $this->addSql('CREATE INDEX IDX_BE2DDF8C70314C6C ON produits (etp_id_id)');
        $this->addSql('CREATE INDEX IDX_BE2DDF8C57BF3EAA ON produits (gamme_id_id)');
        $this->addSql('CREATE INDEX IDX_BE2DDF8C30964F9C ON produits (marque_id_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE produit_extension DROP FOREIGN KEY FK_1B20639BF347EFB');
        $this->addSql('ALTER TABLE produit_extension DROP FOREIGN KEY FK_1B20639B44AEE5AE');
        $this->addSql('DROP INDEX IDX_1B20639BF347EFB ON produit_extension');
        $this->addSql('DROP INDEX IDX_1B20639B44AEE5AE ON produit_extension');
        $this->addSql('ALTER TABLE produit_extension DROP produit_id, DROP parametres_id');
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8C70314C6C');
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8C57BF3EAA');
        $this->addSql('ALTER TABLE produits DROP FOREIGN KEY FK_BE2DDF8C30964F9C');
        $this->addSql('DROP INDEX IDX_BE2DDF8C70314C6C ON produits');
        $this->addSql('DROP INDEX IDX_BE2DDF8C57BF3EAA ON produits');
        $this->addSql('DROP INDEX IDX_BE2DDF8C30964F9C ON produits');
        $this->addSql('ALTER TABLE produits ADD etp_id INT DEFAULT NULL, ADD gamme_id INT DEFAULT NULL, ADD marque_id INT DEFAULT NULL, DROP etp_id_id, DROP gamme_id_id, DROP marque_id_id, CHANGE type_id type_id INT DEFAULT NULL, CHANGE pdt_description pdt_description VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE pdt_image pdt_image VARCHAR(50) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8C70314C6C FOREIGN KEY (etp_id) REFERENCES etat_produit (id)');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8C57BF3EAA FOREIGN KEY (gamme_id) REFERENCES gamme_produit (id)');
        $this->addSql('ALTER TABLE produits ADD CONSTRAINT FK_BE2DDF8C30964F9C FOREIGN KEY (marque_id) REFERENCES marque_produit (id)');
        $this->addSql('CREATE INDEX IDX_BE2DDF8C70314C6C ON produits (etp_id)');
        $this->addSql('CREATE INDEX IDX_BE2DDF8C57BF3EAA ON produits (gamme_id)');
        $this->addSql('CREATE INDEX IDX_BE2DDF8C30964F9C ON produits (marque_id)');
    }
}
