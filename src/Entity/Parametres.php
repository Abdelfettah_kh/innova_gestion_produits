<?php

namespace App\Entity;

use App\Repository\ParametresRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParametresRepository::class)
 */
class Parametres
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $par_parametre;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $par_valeur;

    /**
     * @ORM\OneToMany(targetEntity=ProduitExtension::class, mappedBy="parametres")
     */
    private $param;

    public function __construct()
    {
        $this->param = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParParametre(): ?string
    {
        return $this->par_parametre;
    }

    public function setParParametre(string $par_parametre): self
    {
        $this->par_parametre = $par_parametre;

        return $this;
    }

    public function getParValeur(): ?string
    {
        return $this->par_valeur;
    }

    public function setParValeur(string $par_valeur): self
    {
        $this->par_valeur = $par_valeur;

        return $this;
    }

    /**
     * @return Collection|ProduitExtension[]
     */
    public function getParam(): Collection
    {
        return $this->param;
    }

    public function addParam(ProduitExtension $param): self
    {
        if (!$this->param->contains($param)) {
            $this->param[] = $param;
            $param->setParametres($this);
        }

        return $this;
    }

    public function removeParam(ProduitExtension $param): self
    {
        if ($this->param->contains($param)) {
            $this->param->removeElement($param);
            // set the owning side to null (unless already changed)
            if ($param->getParametres() === $this) {
                $param->setParametres(null);
            }
        }

        return $this;
    }
}
